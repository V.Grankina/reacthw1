import React from "react";

class Modal extends React.Component {
    render() {
        const {show, header,text,closeBtn,onClose, actions} = this.props;
        if(!show){
            return null;
        }
        return (
            <div className="modal" onClick={(e)=>{
                if(e.target.classList.contains('modal')){
                    onClose()
                }
            }}>
                <div className="modal-content">
                    <div className="modal-header">
                        <span className="close" onClick={onClose} hidden={!closeBtn}>&times;</span>
                        <h2>{header}</h2>
                    </div>
                    <div className="modal-body">
                        {text}
                    </div>
                    {actions}
                </div>
            </div>
        );
    }
}

export default Modal;