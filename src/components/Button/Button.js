import React from "react";

function Button ({bgColor, text, onClick}){
    return <button type="button" onClick={onClick} style={{backgroundColor: bgColor}}>{text}</button>
}

export default Button;