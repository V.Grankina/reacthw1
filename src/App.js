import React from "react";
import Button from "./components/Button/Button";
import variables from './components/variables.scss'
import Modal from "./components/Modal/Modal";

class App extends React.Component {
    state = {
        showModal: false,
    }

    openFirstModal = () => {
        this.setState({
            showModal: true,
            modalHeaderText: "Do you want to delete this file?",
            modalBodyText: "Once you delete this file, it won't be possible to undo this action." +
                " Are you sure you want to delete it?",
            modalCloseBtn: true,
            actions: <div className="modal-footer">
                <Button bgColor={variables.colorRed} onClick={this.closeModal} text="OK"/>
                <Button bgColor={variables.colorRed} onClick={this.closeModal} text="Cancel"/>
            </div>,
        })
    }
    openSecondModal = () => {
        this.setState({
            showModal: true,
            modalHeaderText: "React rocks!",
            modalBodyText: <p> Roses are red <br/>
                Violets are blue <br/>
                Unexpected "&#123;" on line 32 </p>,
            modalCloseBtn: false,
            actions: <div className="modal-footer">
                <Button bgColor={variables.colorRed} onClick={this.closeModal} text="Cool"/>
            </div>,
        })
    }

    closeModal = () => {
        this.setState({
            showModal: false,
            modalHeaderText: "",
            modalBodyText: "",
            modalCloseBtn: false,
        })

    }
    render = () => {
        return <>
            <Button text="Open first modal" bgColor={variables.colorRed} onClick={this.openFirstModal}/>
            <Button text="Open second modal" bgColor={variables.colorGreen} onClick={this.openSecondModal}/>
            <Modal
                show={this.state.showModal}
                header={this.state.modalHeaderText}
                text={this.state.modalBodyText}
                closeBtn={this.state.modalCloseBtn}
                onClose={this.closeModal}
                actions={this.state.actions}/>
        </>
    };
}

export default App